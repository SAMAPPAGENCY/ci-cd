<!-- Branche Master -->
[![pipeline status](https://gitlab.com/SAMAPPAGENCY/ci-cd/badges/master/pipeline.svg)](https://gitlab.com/SAMAPPAGENCY/ci-cd/-/commits/master) 
[![coverage report](https://gitlab.com/SAMAPPAGENCY/ci-cd/badges/master/coverage.svg)](https://gitlab.com/SAMAPPAGENCY/ci-cd/-/commits/master)


commande pour le coverage sans configurer php.ini :
XDEBUG_MODE=coverage ./vendor/bin/phpunit

Commande pour lancer sonar-scanner :
```bash
sonar-scanner \
  -Dsonar.projectKey=mspr-ci-cd \
  -Dsonar.sources=. \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=48ce8c21ecc4fa90ef4790f49578ae3b19387446
```