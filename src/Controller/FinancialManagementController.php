<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller dédié à la gestion financière
 * 
 * **Description:** Controller dédié à la gestion financière
 *  1. Gestion des coûts
 *  2. Valorisaitobn
 *  3. etc ...
 * 
 * @author samir founou <samir_615@live.fr> 
 */
class FinancialManagementController extends AbstractController
{
    #[Route('/financial/management', name: 'financial_management')]
    public function index(): Response
    {
        return $this->render('financial_management/index.html.twig', [
            'controller_name' => 'FinancialManagementController',
        ]);
    }
}
