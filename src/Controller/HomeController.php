<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller dédié à la page d'accueil
 * 
 * **Description:** Controller dédié à la page d'accueil
 *  1. Affichage des services
 *  2. Liens de connexion
 * 
 * @author samir founou <samir_615@live.fr> 
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
