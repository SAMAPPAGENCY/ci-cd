<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller dédié à la gestion des ressources humaines
 * 
 * **Description:** Controller dédié à la gestion des ressources humaines
 *  1. Finances
 *  2. Stocks
 *  3. Production
 * 
 * @author samir founou <samir_615@live.fr> 
 */
class HumanResourceController extends AbstractController
{
    #[Route('/human/resource', name: 'human_resource')]
    public function index(): Response
    {
        return $this->render('human_resource/index.html.twig', [
            'controller_name' => 'HumanResourceController',
        ]);
    }
}
