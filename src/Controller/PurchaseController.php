<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller dédié à la gestion des achats
 * 
 * **Description:** Controller dédié à la gestion des achats
 *  1. Gestion des prix
 *  2. Gestion des commandes
 *  3. Qualité
 *  4. etc ...
 * 
 * @author samir founou <samir_615@live.fr> 
 */
class PurchaseController extends AbstractController
{
    #[Route('/purchase', name: 'purchase')]
    public function index(): Response
    {
        return $this->render('purchase/index.html.twig', [
            'controller_name' => 'PurchaseController',
        ]);
    }
}
