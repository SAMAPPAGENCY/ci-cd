<?php

namespace App\Entity;

/**
 * Bank entity
 * @author Samir Founou <email@email.com>
 */
class Bank
{
    /**
     * @var int
     */
    private $id;

     /**
     * @var string
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
