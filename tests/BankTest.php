<?php

namespace App\Tests;

use App\Entity\Bank;
use PHPUnit\Framework\TestCase;

class BankTest extends TestCase
{

    public function testIfBankHaveId()
    {
        $bank = new Bank();
        $bank->setId(1);

        $this->assertEquals(1, $bank->getId());
    }

    public function testIfBankHaveName()
    {
        $bank = new Bank();
        $bank->setName("HSBC");

        $this->assertEquals("HSBC", $bank->getName());
    }
}
